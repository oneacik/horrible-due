// eslint-disable-next-line no-unused-vars

import {RegisterDTO} from '../../common/RegisterDTO';

import {Login} from '../../common/User';

import {DatabaseService} from './DatabaseService';

let databaseService: DatabaseService;

const registerDTO = new RegisterDTO(
  'Piotr',
  'Suwała',
  'oneat@vp.pl',
  '662117872',
  'Strumykowa 16',
  '69-420',
  'Danzig'
);

beforeEach(async () => {
  databaseService = new DatabaseService();
  await databaseService.getInstance({
    client: 'sqlite3',
    connection: ':memory:',
    useNullAsDefault: true
  });

  await databaseService.save(registerDTO);
});

afterEach(() => databaseService.getInstance()
  .then(x => x.destroy()));

it('test getAll', async () => {
  const result = await databaseService.getAll();

  expect(result).toHaveLength(1);

  expect(result[0].id).toBe(1);
  expect(result[0].first_name).toBe(registerDTO.firstName);
  expect(result[0].last_name).toBe(registerDTO.lastName);

  expect(result[0].email).toBe(registerDTO.email);
  expect(result[0].telephone).toBe(registerDTO.telephone);

  expect(result[0].address).toBe(registerDTO.address);
  expect(result[0].city).toBe(registerDTO.city);
  expect(result[0].postcode).toBe(registerDTO.postcode);
});


it('test correctQuery', async () => {
  const result = await databaseService.query(new Login(registerDTO.firstName, registerDTO.lastName));

  expect(result).toHaveLength(1);

  expect(result[0].id).toBe(1);
  expect(result[0].first_name).toBe(registerDTO.firstName);
  expect(result[0].last_name).toBe(registerDTO.lastName);

  expect(result[0].email).toBe(registerDTO.email);
  expect(result[0].telephone).toBe(registerDTO.telephone);

  expect(result[0].address).toBe(registerDTO.address);
  expect(result[0].city).toBe(registerDTO.city);
  expect(result[0].postcode).toBe(registerDTO.postcode);
});

it('test IncorrectQuery', async () => {
  const result = await databaseService.query(new Login('JEBAĆ', 'PiS'));

  expect(result).toHaveLength(0);
});
