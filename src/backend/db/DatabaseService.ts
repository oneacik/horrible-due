import knex from 'knex';
import {RegisterDTO} from "../../common/RegisterDTO";
import {Login} from "../../common/User";

export class DatabaseService {
  instance: any = undefined;


  async getInstance(configuration: any =
                      {
                        client: 'sqlite3',
                        useNullAsDefault: true,
                        connection: {
                          filename: "./cringe.sqlite"
                        }
                      }
  ) {
    if (this.instance == undefined) {
      this.instance = knex(configuration);

      console.log('Creating Schema');

      await this.instance.schema.createTable('users', (table: any) => {
        table.increments('id');
        table.string("first_name");
        table.string("last_name");
        table.string("email");
        table.string("telephone");
        table.string("address");
        table.string("postcode");
        table.string("city");
      })

      console.log('Created Schema');
    }

    return this.instance;
  }

  async save(registerDto: RegisterDTO) {
    console.log("REGISTERING!")
    console.log(registerDto);

    const possibleUsers = await this.query(new Login(registerDto.firstName, registerDto.lastName));

    if (possibleUsers.length != 0) {
      return possibleUsers[0];
    }

    return this.getInstance()
      .then(instance => instance('users').insert(new UserDAO(
        null,
        registerDto.firstName,
        registerDto.lastName,
        registerDto.email,
        registerDto.telephone,
        registerDto.address,
        registerDto.postcode,
        registerDto.city
      )));
  }

  async getAll(): Promise<UserDAO[]> {
    return this.getInstance()
      .then(instance => instance('users').select());
  }

  async query(login: Login): Promise<UserDAO[]> {
    console.log('Querying by Login');
    return this.getInstance()
      .then(instance => instance('users').select().where({
        first_name: login.firstName,
        last_name: login.lastName
      }));
  }


}


export class UserDAO {
  constructor(public id: number | null,
              public first_name: string,
              public last_name: string,
              public email: string,
              public telephone: string,
              public address: string,
              public postcode: string,
              public city: string) {
  }

}
