// eslint-disable-next-line no-unused-vars
import * as path from 'path';


// eslint-disable-next-line no-unused-vars
import {Express} from 'express';

import {CSVService} from '../service/CSVService';
// eslint-disable-next-line no-unused-vars
import {Login, LoginResponse} from '../../common/User';

export default async function (app: Express) {
  const csvService = new CSVService();
  const csvData = await csvService.retrieveCSV(path.join(__dirname, '..', '..', '..', 'static/test.csv'));

  app.post('/user/verify', (req, res) => {
    console.log('user verified');
    const login: Login = req.body;
    console.log(login);
    res.send(csvData.verify(login));
  });

  app.post('/user/due', (req, res) => {
    console.log('user dued');
    const login: Login = req.body;
    console.log(login);
    res.send(csvData.getDue(login));
  });
}
