// eslint-disable-next-line no-unused-vars
import * as path from 'path';

// eslint-disable-next-line no-unused-vars
import {Express} from 'express';

// eslint-disable-next-line no-unused-vars
import {RegisterDTO} from '../../common/RegisterDTO';
import {DatabaseService} from '../db/DatabaseService';
import {Login} from '../../common/User';

export default async function (app: Express) {
  const databaseService = new DatabaseService();

  app.post('/register/add', (req, res) => {
    console.log('user verified');
    const registerDTO: RegisterDTO = req.body;

    databaseService.save(registerDTO)
      // eslint-disable-next-line no-magic-numbers
      .then(() => res.sendStatus(201))
      // eslint-disable-next-line no-magic-numbers
      .catch(err => res.status(500).send(err));
  });

  app.get('/register/users', (req, res) => {
    databaseService.getAll()
      .then(data => res.send(data))
      .catch(err => {
        console.error(err);
        // eslint-disable-next-line no-magic-numbers
        res.status(500).send(err);
      });
  });

  app.post('/register/exists', (req, res) => {
    console.log('user exists');
    const login: Login = req.body;

    databaseService.query(login).then(x => {
      if (x.length !== 0) {
        res.send(true);
      } else {
        res.send(false);
      }
    })
      // eslint-disable-next-line no-magic-numbers
      .catch(err => res.status(500).send(err));
  });
}
