import * as path from 'path';

// eslint-disable-next-line no-unused-vars
import express, {Express} from 'express';

// eslint-disable-next-line no-unused-vars
import {Login, LoginResponse} from '../common/User';

import csvRouter from './routers/CsvRouter';
import registerRouter from './routers/RegisterRouter';


(async function main() {
  const app: Express = express();
  const port = 3000;

  app.use(express.json());

  await csvRouter(app);
  await registerRouter(app);

  app.use(
    '/app/',
    express.static(path.join(__dirname, '../../dist'), {
      setHeaders(res) {
        res.set('Access-Control-Allow-Origin', '*');
      }
    })
  );

  app.use(
    '/static/',
    express.static(path.join(__dirname, '../../static'), {
      setHeaders(res) {
        res.set('Access-Control-Allow-Origin', '*');
      }
    })
  );

  app.get('/', (req, res) => res.redirect('/app'));

  app.listen(port, () => console.log(`Wallboard running on port: ${port}!`));
}());

