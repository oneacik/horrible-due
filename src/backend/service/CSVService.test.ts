import * as path from 'path';

import {CSVService} from './CSVService';

it('CSV can load CSV lol xD', async () => {
  const csv = new CSVService();
  const returnedCSV = await csv.retrieveCSV(path.join(__dirname, '..', '..', '..', 'static/test.csv'));

  expect(returnedCSV.paymentInfos).toHaveLength(1);

  expect(returnedCSV.paymentInfos[0].firstName).toBe('Piotr');
  expect(returnedCSV.paymentInfos[0].lastName).toBe('Suwała');
  expect(returnedCSV.paymentInfos[0].due).toBe('21.37');
});
