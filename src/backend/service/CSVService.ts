import * as path from 'path';

import csv from 'csvtojson';

// eslint-disable-next-line no-unused-vars
import {Login, LoginResponse} from '../../common/User';

export class PaymentInfosDAOs {
  // eslint-disable-next-line no-unused-vars
  constructor(public paymentInfos: PaymentInfoDAO[]) {
  }

  verify(login: Login): LoginResponse {
    return new LoginResponse(
      this.paymentInfos.find(x => x.firstName === login.firstName) !== undefined,
      this.paymentInfos.find(x => x.lastName === login.lastName) !== undefined,
      this.paymentInfos.find(x => x.firstName === login.firstName && x.lastName === login.lastName) !== undefined,
    );
  }

  getDue(login: Login) {
    const due = this.paymentInfos
      .find(x => x.firstName === login.firstName && x.lastName === login.lastName);
    return due !== undefined ? due.due : undefined;
  }
}

export class PaymentInfoDAO {
  constructor(
    public firstName: string,
    public lastName: string,
    public due: number) {
  }


}


export class CSVService {
  async retrieveCSV(file: string): Promise<PaymentInfosDAOs> {
    const csvPath = file;
    console.warn(csvPath);

    return new PaymentInfosDAOs(
      await (csv().fromFile(csvPath) as unknown as PaymentInfoDAO[])
    );
  }
}

