export class RegisterDTO {
  constructor(public firstName: string,
              public lastName: string,
              public email: string,
              public telephone: string,
              public address: string,
              public postcode: string,
              public city: string
  ) {
  }
}
