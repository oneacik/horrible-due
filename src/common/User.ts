export class LoginResponse {
  constructor(public isFirstNameExisting: boolean,
              public isSecondNameExisting: boolean,
              public isFullNameMatching: boolean
  ) {
  }
}

export class Login {
  constructor(public firstName: string,
              public lastName: string
  ) {
  }
}
