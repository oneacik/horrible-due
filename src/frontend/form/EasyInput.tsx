import React from 'react';
import Form from 'react-bootstrap/Form';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

import 'bootstrap/dist/css/bootstrap.css';
import {Observable, Subject} from 'rxjs';
import {filter} from 'rxjs/operators';


export class InputConfig {
  // eslint-disable-next-line no-unused-vars
  constructor(
    public name: string,
    public label: string
  ) {
  }
}

export class Value {
  constructor(
    public name: string,
    public value: string
  ) {
  }
}

class EasyInputProps {
  constructor(
    public name: string,
    public label: string,
    public valueCallback: Subject<Value>,
    public validationHandler: Subject<ValidationError>,
  ) {
  }
}

class EasyInputState {
  constructor(
    public content: string,
    public isValid: boolean,
    public isInvalid: boolean,
    public error: string | undefined
  ) {
  }
}

export class ValidationError {
  constructor(
    public name: string,
    public valid: boolean,
    public error: string | undefined
  ) {
  }
}


export class EasyInput
  extends React.Component
    <EasyInputProps, EasyInputState> {

  constructor(props: EasyInputProps) {
    super(props);
    this.state = new EasyInputState('', false, false, undefined);
    props.valueCallback.next(new Value(this.props.name, ''));
    props.validationHandler
      .pipe(filter(validation =>
        validation.name === this.props.name
      ))
      .subscribe(validation => {
        this.setState({isValid: validation.valid, isInvalid: !validation.valid, error: validation.error});
      });
  }

  // eslint-disable-next-line max-len
  render() {
    return (
      <Form.Group as={Row}>
        <Form.Label column sm="4">
          {this.props.label}
        </Form.Label>
        <Col sm="8">
          <Form.Control
            type="text"
            id={this.props.name}
            placeholder={this.props.label}
            value={this.state.content}
            isValid={this.state.isValid}
            isInvalid={this.state.isInvalid}
            onChange={(event: any) => {
              this.setState({content: event.target.value});
              this.props.valueCallback.next(new Value(this.props.name, event.target.value));
            }}
          />
          <Form.Control.Feedback type="invalid">
            {this.state.error}
          </Form.Control.Feedback>
        </Col>
      </Form.Group>
    );
  }
}

export function createInputs(
  inputs: InputConfig[],
  valueCallback: Subject<Value>,
  validationHandler: Subject<ValidationError>
) {
  return inputs
    .map(input => (
      createInput(input, valueCallback, validationHandler)
    ));
}

export function createInput(
  input: InputConfig,
  valueCallback: Subject<Value>,
  validationHandler: Subject<ValidationError>
) {
  return (
    <EasyInput
      name={input.name}
      key={input.name}
      label={input.label}
      validationHandler={validationHandler}
      valueCallback={valueCallback}
    />
  );
}


export function createSimpleValueCallback(that: React.Component<any, { data: any }>): Subject<Value> {
  const valueCallback: Subject<Value> = new Subject();
  valueCallback.subscribe(value => {
    console.log(`received text: ${value.value}`);
    that.setState(state => {
      state.data[value.name] = value.value;
      return state;
    });
  });

  return valueCallback;
}
