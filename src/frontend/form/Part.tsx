import React from 'react';

import {Observable, Subject} from 'rxjs';

import {verifyLogin} from '../service/UserService';

// eslint-disable-next-line no-unused-vars
import {LoginResponse} from '../../common/User';

import {createInputs, createSimpleValueCallback, EasyInput, InputConfig, ValidationError} from './EasyInput';

// eslint-disable-next-line no-unused-vars
import {RespublicaForm} from './RespublicaForm';

export interface Passing {
}

// eslint-disable-next-line no-unused-vars
export class PartProps {
  constructor(public fromSubject: Subject<Passing>,
              public toSubject: Subject<Passing>) {
  }
}

export class PassingValue implements Passing {
  constructor(public value: object) {
  }
}

export class PassingError implements Passing {
  constructor(public errors: string[]) {
  }
}

export class PassingDue implements Passing {
  constructor(public due: number) {
  }
}


export abstract class Part
  extends React.Component
    <any, any> {

  constructor(props: PartProps) {
    super(props);
    this.validationHandler = new Subject<ValidationError>();
    this.state = {data: {}, enabled: false};
    this.setObservers();
  }

  protected validationHandler: Subject<ValidationError>;

  setObservers() {
    this.props.fromSubject
      .subscribe({
        // eslint-disable-next-line complexity
        next: async (forward: object) => {
          if (!this.state.enabled) {
            if (forward instanceof PassingValue) {
              this.setState({enabled: true});
            }

            if (forward instanceof PassingError) {
              this.props.toSubject.next(forward);
            }

            return;
          }

          if (forward instanceof PassingValue) {
            const error = await this.validate(this.validationHandler);
            if (error === undefined) {
              console.log('propagating event');
              this.props.toSubject.next(new PassingValue({...forward.value, ...this.populate()}));
            } else {
              console.log('error occured');
              this.props.toSubject.next(new PassingError([error]));
            }
          }

          if (forward instanceof PassingError) {
            console.log('propagating error');
            const error = await this.validate(this.validationHandler);
            this.props.toSubject.next(
              new PassingError([...(error === undefined ? [`${error}`] : []), ...forward.errors])
            );
          }
        }
      });
  }

  /*
   * Validates fields
   * @return string | undefined containing the error or undefined if none
   */
  abstract async validate(validator: Subject<ValidationError>): Promise<string | undefined>;

  /*
   * Returns inputConfigs of current part
   */
  abstract getInputs(): InputConfig[];

  populate(): object {
    return this.state.data;
  }

  render(): any {
    return this.state.enabled
      ? createInputs(this.getInputs(), createSimpleValueCallback(this), this.validationHandler)
      : null;
  }
}


