import React from 'react';
import Form from 'react-bootstrap/Form';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button';

import 'bootstrap/dist/css/bootstrap.css';


// eslint-disable-next-line no-unused-vars
// @ts-ignore
import '../css/Main.css';
import Modal from 'react-bootstrap/Modal';


import {Observable, Subject} from 'rxjs/Rx';

import Alert from 'react-bootstrap/Alert';

import {getDue, registerUser} from '../service/UserService';

import {RegisterDTO} from '../../common/RegisterDTO';

import {createInput, createSimpleValueCallback, ValidationError, Value, InputConfig} from './EasyInput';
import {LoginPart} from './parts/LoginPart';
import {PassingDue, PassingError, PassingValue} from './Part';
import {ContactPart} from './parts/ContactPart';
import {DormitoryPart} from './parts/DormitoryPart';

// eslint-disable-next-line no-unused-vars


export class RespublicaForm
  extends React.Component<any, any> {


  constructor(props: any) {
    super(props);

    this.state = {
      onSubmits: [],
      data: {},
      alert: [],
      due: undefined
    };
    this.ksi = createSimpleValueCallback(this);

    this.fromSubject = new Subject<object>();
    this.midSubject = new Subject<object>();
    this.mid2Subject = new Subject<object>();
    this.toSubject = new Subject<object>();
    this.toSubject.subscribe((x: object) => {
      if (x instanceof PassingValue) {
        const passingValue: PassingValue = x as PassingValue;
        const registerDTO: RegisterDTO = passingValue.value as RegisterDTO;

        registerUser(registerDTO)
          .then(() => console.log(passingValue.value))
          .then(() => getDue(registerDTO.firstName, registerDTO.lastName))
          .then(due => this.setState({due}));
      }

      if (x instanceof PassingError) {
        this.setState({alert: x.errors});
      }

      if (x instanceof PassingDue) {
        this.setState({due: x.due});
      }
    });
  }

  public ksi: Subject<Value>;

  private fromSubject: Subject<object>;
  private midSubject: Subject<object>;
  private mid2Subject: Subject<object>;
  private toSubject: Subject<object>;

  submit() {
    this.setState({alert: []});
    this.fromSubject.next(new PassingValue({}));
  }

  render(): any { // @TODO add spinner
    return (
      <div id={'main'}>
        {
          <Modal.Dialog>
            <Modal.Header>
              <Modal.Title>{'Sprawdź stan środków'}</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              {(this.state.due === undefined)
                ? undefined
                : (
                  <Alert variant="success">
                    {`Kwota do zapłaty wynosi: ${this.state.due}`}
                  </Alert>
                )}
              {this.state.alert.length === 0
                ? undefined
                : (
                  <Alert variant="danger">
                    {this.state.alert.map((x: string) => (<>{x}<br/></>))}
                  </Alert>
                )
              }
              {(this.state.due === undefined)
                ? (
                  <Form>
                    <LoginPart fromSubject={this.fromSubject} toSubject={this.midSubject} endSubject={this.toSubject}/>
                    <ContactPart fromSubject={this.midSubject} toSubject={this.mid2Subject}/>
                    <DormitoryPart fromSubject={this.mid2Subject} toSubject={this.toSubject}/>
                    <Button variant="primary" id="submit" type="button" onClick={() => this.submit()}>
                      {'Zatwierdź'}
                    </Button>
                  </Form>
                )
                : undefined}
            </Modal.Body>
          </Modal.Dialog>
        }
      </div>
    );
  }
}
