import React from 'react';

import {Subject} from 'rxjs';

import {verifyLogin} from '../../service/UserService';

// eslint-disable-next-line no-unused-vars
import {LoginResponse} from '../../../common/User';

import {createInputs, EasyInput, InputConfig, ValidationError} from '../EasyInput';

// eslint-disable-next-line no-unused-vars
import {RespublicaForm} from '../RespublicaForm';
import {Part, PartProps} from '../Part';

export class ContactPart extends Part {

  constructor(props: PartProps) {
    super(props);
  }

  getInputs(): InputConfig[] {
    return [
      new InputConfig('email', 'e-mail'),
      new InputConfig('telephone', 'telefon')
    ];
  }

  async validate(validator: Subject<ValidationError>): Promise<string | undefined> {
    if (!this.state.data.email) {
      validator.next(new ValidationError('email', false, 'Brakujący email'));
    } else {
      validator.next(new ValidationError('email', true, undefined));
    }

    if (!this.state.data.telephone) {
      validator.next(new ValidationError('telephone', false, 'Brakujący telefon'));
    } else {
      validator.next(new ValidationError('telephone', true, undefined));
    }

    return (!this.state.data.email || !this.state.data.telephone) ? 'Uzupełnij Brakujące Dane Kontaktowe' : undefined;
  }
}
