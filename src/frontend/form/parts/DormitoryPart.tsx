import React from 'react';

import {Subject} from 'rxjs';

import {verifyLogin} from '../../service/UserService';

// eslint-disable-next-line no-unused-vars
import {LoginResponse} from '../../../common/User';

import {createInputs, EasyInput, InputConfig, ValidationError} from '../EasyInput';

// eslint-disable-next-line no-unused-vars
import {Part, PartProps} from '../Part';

export class DormitoryPart extends Part {

  constructor(props: PartProps) {
    super(props);
  }

  getInputs(): InputConfig[] {
    return [
      new InputConfig('address', 'adres'),
      new InputConfig('postcode', 'kod pocztowy'),
      new InputConfig('city', 'miasto')

    ];
  }

  async validate(validator: Subject<ValidationError>): Promise<string | undefined> {
    if (!this.state.data.address) {
      validator.next(new ValidationError('address', false, 'Brakujący adres'));
    } else {
      validator.next(new ValidationError('address', true, undefined));
    }

    if (!this.state.data.postcode) {
      validator.next(new ValidationError('postcode', false, 'Brakujący kod pocztowy'));
    } else {
      validator.next(new ValidationError('postcode', true, undefined));
    }

    if (!this.state.data.city) {
      validator.next(new ValidationError('city', false, 'Brakujące miasto'));
    } else {
      validator.next(new ValidationError('city', true, undefined));
    }

    return (!this.state.data.address || !this.state.data.postcode || !this.state.data.city)
      ? 'Uzupełnij Brakujące Dane Kontaktowe'
      : undefined;
  }
}
