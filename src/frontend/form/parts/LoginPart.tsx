import React from 'react';

import {Subject} from 'rxjs';

import {existsUser, getDue, verifyLogin} from '../../service/UserService';

// eslint-disable-next-line no-unused-vars
import {Login, LoginResponse} from '../../../common/User';

import {createInputs, EasyInput, InputConfig, ValidationError} from '../EasyInput';

// eslint-disable-next-line no-unused-vars
import {RespublicaForm} from '../RespublicaForm';
import {Part, PartProps, Passing, PassingDue, PassingError} from '../Part';

class PartPropsExtended {
  constructor(public fromSubject: Subject<Passing>,
              public toSubject: Subject<Passing>,
              public endSubject: Subject<Passing>
  ) {
  }
}

export class LoginPart extends Part {

  constructor(props: PartPropsExtended) {
    const midSubject: Subject<object> = new Subject<object>();
    props.fromSubject.subscribe(x => {
      existsUser(new Login(this.state.data.firstName, this.state.data.lastName))
        .then(exists => {
          if (!exists) {
            midSubject.next(x);
          } else {
            getDue(this.state.data.firstName, this.state.data.lastName)
              .then(due => props.endSubject.next(new PassingDue(due)));
          }
        });
    });
    super(new PartProps(midSubject, props.toSubject));
    // @ts-ignore
    this.state.enabled = true;
  }

  getInputs(): InputConfig[] {
    return [
      new InputConfig('firstName', 'imię'),
      new InputConfig('lastName', 'nazwisko')
    ];
  }

  async validate(validator: Subject<ValidationError>): Promise<string | undefined> {
    const response: LoginResponse = await verifyLogin(this.state.data.firstName, this.state.data.lastName);

    if (!response.isFirstNameExisting) {
      validator.next(new ValidationError('firstName', false, 'Nie ma takiego imienia w bazie'));
    } else {
      validator.next(new ValidationError('firstName', true, undefined));
    }

    if (!response.isSecondNameExisting) {
      validator.next(new ValidationError('lastName', false, 'Nie ma takiego nazwiska w bazie'));
    } else {
      validator.next(new ValidationError('lastName', true, undefined));
    }


    return response.isFullNameMatching ? undefined : 'Imię i nazwisko nie pasuje do użytkownika w bazie';
  }
}
