import axios from 'axios';

import {Login, LoginResponse} from '../../common/User';
import {RegisterDTO} from '../../common/RegisterDTO';

const HTTP_OK = 200;
const HTTP_CREATED = 201;

export function getDue(firstName: string, lastName: string): Promise<number> {
  return axios.post('/user/due', new Login(firstName, lastName))
    .then(x => (x.status !== HTTP_OK ? new Error(`Received ${x.status} on verifyLogin`) : x.data));
}

export function verifyLogin(firstName: string, lastName: string): Promise<LoginResponse> {
  return axios.post('/user/verify', new Login(firstName, lastName))
    .then(x => (x.status !== HTTP_OK ? new Error(`Received ${x.status} on verifyLogin`) : x.data));
}

export function registerUser(registerDTO: RegisterDTO): Promise<object> {
  return axios.post('/register/add', registerDTO)
    .then(x => (x.status !== HTTP_CREATED ? new Error(`Received ${x.status} on registerUser`) : x.data));
}

export function existsUser(login: Login): Promise<boolean> {
  return axios.post('/register/exists', login)
    .then(x => (x.status !== HTTP_OK ? new Error(`Received ${x.status} on existsUser`) : x.data));
}
