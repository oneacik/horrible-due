// eslint-disable-next-line no-unused-vars
import {Builder, By, Key, until, WebDriver} from 'selenium-webdriver';


async function findAndWrite(driver: WebDriver, name: string, valueToWrite: string) {
  await driver.wait(until.elementLocated(By.id(name)));
  await driver.findElement(By.id(name)).sendKeys(valueToWrite);
}

test('Can process correct path', async () => {


  const driver = new Builder()
    .forBrowser('chrome')
    .build();

  await driver.get('localhost:3000');
  await findAndWrite(driver, 'first_name', 'Piotr');
  await findAndWrite(driver, 'last_name', 'Suwała');
  await driver.findElement(By.id('submit')).click();

  await findAndWrite(driver, 'email', 'piotr.suwala@respublica.org.pl');
  await findAndWrite(driver, 'telephone', '662117872');
  await driver.findElement(By.id('submit')).click();

  await findAndWrite(driver, 'address', 'Karłowicza 61b/1');
  await findAndWrite(driver, 'zipcode', '80-280');
  await findAndWrite(driver, 'city', 'Gdańsk');
  await driver.findElement(By.id('submit')).click();

  await driver.quit();
});
