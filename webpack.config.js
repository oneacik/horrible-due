/* eslint-disable */
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const NodemonPlugin = require('nodemon-webpack-plugin');

const common = x => ({
  ...x,
  resolve: {
    extensions: ['.js', '.ts', '.tsx']
  },
  module: {
    rules: [
      {
        test: /\.css$/i,
        use: ['style-loader', 'css-loader']
      },
      {
        test: /\.tsx?$/,
        exclude: /node_modules/,
        loader: 'ts-loader'
      }
    ]
  }
});

//const backend =;
const frontend = common({
  node: {
    __dirname: false
  },
  entry: './src/frontend/form/App.tsx',
  mode: 'development',
  target: 'web', // change to web if web lol
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'bundle.js'
  },
  plugins: [new HtmlWebpackPlugin()]
});

// const backend = common({
//   node: {
//     __dirname: false
//   },
//   entry: './src/backend/server.ts',
//   mode: 'development',
//   target: 'node', // change to web if web lol
//   output: {
//     path: path.resolve(__dirname, 'bin'),
//     filename: 'server.js'
//   },
//   plugins: [
//     new NodemonPlugin({
//       watch: path.resolve('./src/backend'),
//       ext: 'js,ts,json,twig'
//     })
//   ],
//   externals: {
//     'aws-sdk': 'aws-sdk',
//     pg: 'pg',
//     'pg-query-stream': 'pg-query-stream',
//     mssql: 'mssql',
//     'mssql/lib/base': 'mssql/lib/base',
//     'mssql/package.json': 'mssql/package.json',
//     mysql: 'mysql',
//     mysql2: 'mysql2',
//     oracle: 'oracle',
//     oracledb: 'oracledb',
//     tedious: 'tedious'
//   }
// });


module.exports = [frontend];
